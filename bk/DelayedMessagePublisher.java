package com.claro.wfm.config;

import lombok.RequiredArgsConstructor;
import org.apache.camel.InOnly;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service("publisher")
@InOnly
public class DelayedMessagePublisher {

    private static final int DELAY = 10000;

    @Autowired
    private final RabbitTemplate template;
    @Autowired
    private final Exchange exchange;

    public void sendDelayedMessage(String msg) {
        MessageEvent messageEvent = new MessageEvent(msg);
        template.convertAndSend(exchange.getName(),messageEvent,
                message -> {
                    message.getMessageProperties().setDelay(DELAY);
                    return message;
                });
    }
}