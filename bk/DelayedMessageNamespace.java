package com.claro.wfm.config;

public final class DelayedMessageNamespace {

    public static final String DELAYED_MESSAGE_QUEUE = "message.delayed";
}