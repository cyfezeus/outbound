package com.claro.wfm.config;

import lombok.Value;

@Value
public class MessageEvent {
    String text;
}