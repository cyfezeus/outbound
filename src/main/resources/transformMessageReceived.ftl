<#ftl ns_prefixes={"urn":"urn:toatech:agent"}>
<#assign aDateTime = .now>
{
  "id": "${doc["urn:send_message/urn:messages/urn:message[1]/urn:message_id"]}",
  "status": "RECEIVED",
  "date" : ${aDateTime?long?c}
}

