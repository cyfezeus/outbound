<#ftl ns_prefixes={"urn":"urn:toatech:agent"}>
<urn:send_message_response xmlns:urn="urn:toatech:agent">
<#list doc["urn:send_message/urn:messages/urn:message"] as message>
    <urn:message_response>
        <urn:message_id>${message["urn:message_id"]}</urn:message_id>
        <urn:status>Recibido</urn:status>
        <urn:description>Mensaje recibido correctamente</urn:description>
    </urn:message_response>
</#list>
</urn:send_message_response>