<#ftl ns_prefixes={"urn":"urn:toatech:agent"}>
<urn:get_message_status_response xmlns:urn="urn:toatech:agent">
    <!--1 or more repetitions:-->
    <#list doc["urn:get_message_status/urn:messages/urn:message"] as message>
        <urn:message_response>
            <urn:message_id>${message["urn:message_id"]}</urn:message_id>
            <#assign found = false/>
           <#list mdata as dat>
             <#if dat._id?c == message["urn:message_id"]>
                 <#assign found = true/>
                <urn:result>
                    <urn:code>OK</urn:code>
                    <!--Optional:-->
                    <urn:desc>${dat.getString("status")}</urn:desc>
                </urn:result>
             </#if>
           </#list>
            <#if !found>
                <urn:result>
                    <urn:code>NOT FOUND</urn:code>
                    <!--Optional:-->
                    <urn:desc>NOT FOUND</urn:desc>
                </urn:result>
            </#if>
        </urn:message_response>
    </#list>
</urn:get_message_status_response>
