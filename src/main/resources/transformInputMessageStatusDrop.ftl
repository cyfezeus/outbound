<#ftl ns_prefixes={"urn":"urn:toatech:agent"}>
[
{ $match : {$or : [
  <#assign first = true/>
  <#list doc["urn:drop_message/urn:messages/urn:message"] as message>
    <#if first>
       {id: ${message["urn:message_id"]}}
        <#assign first = false/>
    <#else >
       ,{id: ${message["urn:message_id"]}}
    </#if>
  </#list>
  ]}
},
{ $sort: {id: -1, date: 1 }},
{ $group: {
   _id: "$id",
   status: { $last: "$status" },
   date: { $last: "$date" }
}
}
]
