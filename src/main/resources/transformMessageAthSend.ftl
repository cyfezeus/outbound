<#ftl ns_prefixes={"urn":"urn:toatech:agent"}>

<#assign msgError = "${msgError}"/>
 
<urn:send_message_response xmlns:urn="urn:toatech:agent">
	<urn:message_response>
		<urn:description> ${msgError} </urn:description>
	</urn:message_response>
</urn:send_message_response>
