package com.claro.wfm.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;

public class EncryptionAlgorithm {

	public static String sha256(String base) {
	    try{
	    	MessageDigest digest = MessageDigest.getInstance("SHA-256");
	    	byte[] encodedhash = digest.digest(base.getBytes(StandardCharsets.UTF_8));
	        StringBuffer hexString = new StringBuffer();
	        for (int i = 0; i < encodedhash.length; i++) {
	        String hex = Integer.toHexString(0xff & encodedhash[i]);
	        if(hex.length() == 1) hexString.append('0');
	            hexString.append(hex);
	        }
	        return hexString.toString();
	    } catch(Exception ex){
	       throw new RuntimeException(ex);
	    }
	}

}
