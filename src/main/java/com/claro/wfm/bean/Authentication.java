package com.claro.wfm.bean;

import org.apache.camel.Exchange;
import org.springframework.stereotype.Component;

import com.claro.wfm.exception.CustomException;
import com.claro.wfm.utils.EncryptionAlgorithm;
import com.mongodb.BasicDBObject;

import toatech.agent.DropMessageT;
import toatech.agent.SendMessageT;

@Component("authentication")
public class Authentication {

	public void validateUser(BasicDBObject data, Exchange exchange) throws Exception {
		try {
			String login, nowReq, authStringReq, authStringNew, passwordQuery;
			Object requestMessage = new Object();

			// se controla si el usuario llega nulo
			if (data == null)
				throw new Exception("No existe ID USER");

			requestMessage = exchange.getProperty("dataBk");

			if (requestMessage instanceof SendMessageT) {
				// variables de la consulta de metadata
				passwordQuery = data.getString("password");

				// variables del request
				login = ((SendMessageT) requestMessage).getUser().getLogin();
				nowReq = ((SendMessageT) requestMessage).getUser().getNow();
				authStringReq = ((SendMessageT) requestMessage).getUser().getAuthString();

				// encriptacion deacuerdo a OFCS
				authStringNew = EncryptionAlgorithm.sha256(nowReq
						.concat(EncryptionAlgorithm.sha256(passwordQuery.concat(EncryptionAlgorithm.sha256(login)))));

				if (!authStringReq.equalsIgnoreCase(authStringNew)) {
					throw new Exception("El authString no coincide con las credenciales para el user: " + login);
				}

			} else if (requestMessage instanceof DropMessageT) {
				// variables de la consulta de metadata
				passwordQuery = data.getString("password");

				// variables del request
				login = ((DropMessageT) requestMessage).getUser().getLogin();
				nowReq = ((DropMessageT) requestMessage).getUser().getNow();
				authStringReq = ((DropMessageT) requestMessage).getUser().getAuthString();

				// encriptacion deacuerdo a OFCS
				authStringNew = EncryptionAlgorithm.sha256(nowReq
						.concat(EncryptionAlgorithm.sha256(passwordQuery.concat(EncryptionAlgorithm.sha256(login)))));

				if (!authStringReq.equalsIgnoreCase(authStringNew)) {
					throw new Exception("El authString no coincide con las credenciales para el user: " + login);
				}
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

}
