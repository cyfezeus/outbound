package com.claro.wfm.bean;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.springframework.stereotype.Component;

@Component("variable")
public class VariableUtils {
	public Object put(String content, String name, String type, Exchange exchange){
		Map<String, Object> variable = new HashMap<>();
		variable.put("type", type);
		variable.put("content", content);
		exchange.getProperties().put(name, variable);
		return content;
	}

	public void putRef(String variableNameRef, String name, String type, Exchange exchange){
		Object variableRef = get(variableNameRef, exchange);
		Map<String, Object> variable = new HashMap<>();
		variable.put("type", type);
		if(variableRef == null){
			variable.put("content", "");
		}else{
			variable.put("content", variableRef);
		}
		exchange.getProperties().put(name, variable);
	}
	
	public Object get(String name, Exchange exchange){
		return exchange.getProperties().get(name);
	}
}
