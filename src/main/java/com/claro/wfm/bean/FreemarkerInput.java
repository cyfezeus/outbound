package com.claro.wfm.bean;

import org.apache.camel.Exchange;

import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

@Component("freemarkerInput")
public class FreemarkerInput {
	public void addDoc(String name, String data, Exchange exchange){
		Map root = (Map)exchange.getIn().getHeader("CamelFreemarkerDataModel");
		if(root == null) root = new HashMap();
		try {
			InputSource inputSource = new InputSource(new StringReader(data));
			root.put(name, freemarker.ext.dom.NodeModel.parse(inputSource));
			exchange.getIn().setHeader("CamelFreemarkerDataModel", root); 
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addString(String name, String data, Exchange exchange){
		Map root = (Map)exchange.getIn().getHeader("CamelFreemarkerDataModel");
		if(root == null) root = new HashMap();
		root.put(name, data);
		exchange.getIn().setHeader("CamelFreemarkerDataModel", root);
	}

	public void addJSONObject(String name, Object data, Exchange exchange){
		Map root = (Map)exchange.getIn().getHeader("CamelFreemarkerDataModel");
		if(root == null) root = new HashMap();
		root.put(name, data);
		exchange.getIn().setHeader("CamelFreemarkerDataModel", root);
	}


}
