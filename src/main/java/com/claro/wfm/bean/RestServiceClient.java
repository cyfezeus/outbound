package com.claro.wfm.bean;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ws.rs.core.Response;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import org.apache.camel.Exchange;
import org.apache.camel.converter.stream.InputStreamCache;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component("restclient")
public class RestServiceClient {
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
    VariableUtils variable;
	
	@Value("${outbound.endpoint.consume.test}")
	private String endpoint;
	

	private Transformer transformer = null;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestServiceClient.class);
	
	public Object invokeRest(String inputJson, String endpoint, String type) throws Exception {
		try {
			ResponseEntity<String> response = null;
			LOGGER.debug("Invocando rest...");
			HttpHeaders headers = new HttpHeaders();
			headers.add("header_name", "header_value");
			headers.setContentType(MediaType.APPLICATION_JSON);

			//byte[] ptext = inputJson.getBytes(StandardCharsets.ISO_8859_1); 
			//String value = new String(ptext, StandardCharsets.UTF_8); 
			HttpEntity<String> request = new HttpEntity<String>(inputJson, headers);
			switch(type){
				case "POST":
					response = restTemplate.postForEntity(endpoint, request, String.class);
					break;
				case "GET":
					response = restTemplate.getForEntity(endpoint, String.class);
					break;
			}			
			String responseString = new String(response.getBody().getBytes(StandardCharsets.UTF_8.name()));
			
			return responseString;
		} catch (Exception e) {
			LOGGER.debug("ERROR XPATH: " + e.getMessage());
			throw e;
		}
	}

	/*
	public Object execute(String serviceName, String requestName, String responseName, int retry, Exchange exchange) throws Exception{
		String response = "";
        String options[] = serviceName.split(Pattern.quote(":"));
		Map request = (Map) variable.get(requestName, exchange);
		if(request == null || request.isEmpty()){
			throw new Exception("VariableUtils "+ requestName+ " does not exist.");
		}
		WebService webService = webServiceMap.get(options[0]);
		if(webService == null){
			webService = webServiceRepository.findById(options[0]);
			if(webService == null){
				throw new Exception("WebService does not exist: "+options[0]);
			}
			webServiceMap.put(options[0], webService);
		}

		try {
			switch(webService.getType()){
				case "rest":
					response = executeRest(webService, options, (String)request.get("content"), retry);
				    break;
			}
		}catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw e;
		}
		variable.put(response, responseName, webService.getTypeOut(), exchange);
		InputStreamCache responseStream = new InputStreamCache(response.getBytes(StandardCharsets.UTF_8.name()));
		return responseStream;
	}

	private String executeRest(WebService webService, String options[], String request, int retry) throws Exception{
    	String res = "";
    	String endpoint = "";
    	if(transformer == null){
			transformer = TransformerFactory.newInstance().newTransformer();
		}
        if(options.length > 2){
			endpoint = webService.getEndpoint()+(options[2].indexOf("/")==0?options[2]:"/"+options[2]);
		}else{
			endpoint = webService.getEndpoint();
		}
		WebClient client = WebClient.create(endpoint);
		client.type(webService.getProperties().get("Content-Type"));
		client.accept(webService.getProperties().get("Accept"));
		HTTPConduit conduit = WebClient.getConfig(client).getHttpConduit();
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
		httpClientPolicy.setConnectionTimeout(webService.getConnTimeout());
		httpClientPolicy.setAllowChunking(false);
		httpClientPolicy.setReceiveTimeout(webService.getReadTimeout());
		conduit.setClient(httpClientPolicy);
		LOGGER.debug("request: "+webService.getEndpoint() + ": " +request);
		Response response  = null;
		try {
			response  = client.invoke(options[1].toUpperCase(), request);
		}catch(Exception e) {
			long timeToWait = 2000;
			for(int count=1; count<=retry; count++) {
				LOGGER.debug("retrying " + count +": "+webService.getEndpoint() + ": " +request);
				try {
					if(!webService.getProperties().get("timeToWait").isEmpty()) {
						timeToWait = Long.parseLong(webService.getProperties().get("timeToWait"));
					}
					Thread.sleep(timeToWait);
					response  = client.invoke(options[1].toUpperCase(), request);
					count= retry+1;
				}catch(Exception e2) {
					LOGGER.error("error: "+webService.getEndpoint() + ": " +e2.getMessage());
				}
			}
			if(response == null) {
				throw new Exception(ExceptionUtils.getRootCause(e));
			}
		}
		res = response.readEntity(String.class);
		LOGGER.debug("response: "+webService.getEndpoint() + ": " +res);
		return res;
	}
	*/
	
}
