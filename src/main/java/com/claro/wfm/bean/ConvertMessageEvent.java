package com.claro.wfm.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.camel.Exchange;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.claro.wfm.domain.MessageEvent;
import com.claro.wfm.domain.MessageEvent.Status;
import com.mongodb.BasicDBObject;
import com.mongodb.util.JSON;

import toatech.agent.SendMessageT;

@Service("convert")
public class ConvertMessageEvent {

    public MessageEvent toMessageEvent(SendMessageT sendMessage, String status, Exchange exchange){
        MessageEvent message = new MessageEvent();
        message.setId(sendMessage.getMessages().getMessage().get(0).getMessageId());
        message.setDate(new Date());
        byte datab[] = (byte[]) exchange.getProperties().get("data");
        message.setData(new String(datab));
        message.setStatus(Status.valueOf(status));
        return message;
    }

    public MessageEvent toMessageEventDrop(BasicDBObject object, String status, Exchange exchange){
        MessageEvent message = new MessageEvent();
        message.setId(object.getLong("_id"));
        message.setDate(new Date());
        message.setStatus(Status.valueOf(status));
        return message;
    }

    public Object toJSONObject(Object object, Exchange exchange){
        if(object instanceof BasicDBObject){
            JSONObject output = new JSONObject(JSON.serialize(object));
            return output;
        }else if (object instanceof ArrayList){
            List<JSONObject> list = new ArrayList<>();
            for(BasicDBObject bo: (ArrayList<BasicDBObject>)object){
                JSONObject output = new JSONObject(JSON.serialize(bo));
                list.add(output);
            }
            return list;
        }
        return object;
    }

    public String toJSONObjectString(BasicDBObject object, Exchange exchange){
        Object objectConverted = toJSONObject(object, exchange);
        return objectConverted.toString();
    }
    
}
