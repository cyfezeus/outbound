package com.claro.wfm.bean;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("managerFile")
public class ManagerFile {
	
	@Value("${outbound.nameDirectory}")
	private String directoryName;
	
	@Value("${outbound.extensionFile}")
	private String extensionFile;
	
	public void createFile(String targetInvocador, Object data, Exchange exchange) throws Exception {

		BufferedWriter bw = null;
		FileWriter fw = null;
		String fileName, url, endpointName, state, request;

		try {

			fileName = "LOG_MESSAGE_TO_RESEND_"+ targetInvocador;
			
			File directory = new File(this.directoryName);
			
			// Si el archivo no existe, se crea!
			if (!directory.exists()) {
				directory.mkdir();
			}

			url = this.directoryName + fileName + this.extensionFile;
			File file = new File(url);
			
			if (!file.exists()) {
				directory.createNewFile();
			}
			
			// flag true, indica adjuntar información al archivo.
			fw = new FileWriter(file.getAbsoluteFile(), true);
			bw = new BufferedWriter(fw);
			
			state = "FAILED";
			endpointName = exchange.getIn().getHeader("soapOperation").toString();
			request = data.toString();
			
			StringBuffer contenido = new StringBuffer();
			contenido.append("state:"+ state + "|");
			contenido.append("endpointName:"+ endpointName + "|");
			contenido.append("request:" + request);
			contenido.append("\n--------------------------------------\n");
			
			bw.write(contenido.toString());
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
	}
}
