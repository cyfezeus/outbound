package com.claro.wfm.config;

import org.springframework.context.annotation.Configuration;
/*
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;*/



@Configuration
public class KafkaConfig {
	
	 /* @Value("${kafka.bootstrap-servers}")
	  private String bootstrapServers;
	  
	  @Value("${kafka.topic.requestreply-topic}")
	  private String requestReplyTopic;
	  	  
	  @Value("${kafka.group-id}")
	  private String groupId;
	  
	  @Value("${app.kafka.timeout}")
	  private int timeout;
	  
	  @Value("${app.kafka.message-max-size-mb}")
	  private int maxMessage;
	  */
//	  private static Logger logger = LogManager.getLogger(KafkaConfig.class);
	/*
	  @Bean
	  public Map<String, Object> producerConfigs() {
	    Map<String, Object> props = new HashMap<>();
	    // list of host:port pairs used for establishing the initial connections to the Kakfa cluster
	    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
	        bootstrapServers);
	    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
	        StringSerializer.class);
	    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
 	    props.put(ProducerConfig.MAX_REQUEST_SIZE_CONFIG, maxMessage * 1024 * 1024);
	    return props;
	  }
	  
	  @Bean
	  public Map<String, Object> consumerConfigs() {
	    Map<String, Object> props = new HashMap<>();
	    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,bootstrapServers);
	    props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
	    return props;
	  }
	  
	  @Bean
	  public KafkaAdmin admin() {		 
	      Map<String, Object> configs = new HashMap<>();
	      configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG,
	    		  bootstrapServers);
	      return new KafkaAdmin(configs);
	  }
	  
	  @SuppressWarnings("hiding")
	  @Bean
	  public <AdminClient> AdminClient getClient() {
		 Properties properties = new Properties();
		 properties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG,
	    		  bootstrapServers);		 
	    return AdminClient.create(properties);
	  }
	  
	  @Bean
	  public NewTopic topic(AdminClient kafkaAdmin) throws InterruptedException {
		  try {
			  kafkaAdmin.deleteTopics(Arrays.asList(requestReplyTopic)).values().get(requestReplyTopic).get();  
		  }
		  catch( ExecutionException e) {
			  logger.error("El topico no existe, se creara");
		  }
		  NewTopic topic = new NewTopic(requestReplyTopic, 1, (short) 1) ;	  
		  Map<String, String> properties = new HashMap<>();
		  properties.put("max.message.bytes", String.valueOf(maxMessage * 1024 * 1024));		  
		  topic = topic.configs(properties);
		  return topic;		  
	  }

	  @Bean
	  public ProducerFactory<String,String> producerFactory() {		 
	    return new DefaultKafkaProducerFactory<>(producerConfigs());
	  }
	  
	  @Bean
	  public KafkaTemplate<String, String> kafkaTemplate() {
	    return new KafkaTemplate<>(producerFactory());
	  }
	 	  
	  @Bean
	  public KafkaMessageListenerContainer<String, String> replyContainer(ConsumerFactory<String, String> cf) {
	        ContainerProperties containerProperties = new ContainerProperties(requestReplyTopic);
	        return new KafkaMessageListenerContainer<>(cf, containerProperties);
	    }
	  
	  @Bean
	  public ConsumerFactory<String, String> consumerFactory() {
	    return new DefaultKafkaConsumerFactory<>(consumerConfigs(),new StringDeserializer(),new JsonDeserializer<>(String.class));
	  }
	  
	  @Bean
	  public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> kafkaListenerContainerFactory() {
	    ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
	    factory.setConsumerFactory(consumerFactory());
	    factory.setReplyTemplate(kafkaTemplate());
	    return factory;
	  }	  
	  
	  */
}