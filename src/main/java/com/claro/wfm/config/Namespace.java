package com.claro.wfm.config;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.xml.XPathBuilder;
import org.apache.camel.processor.ChoiceProcessor;
import org.apache.camel.processor.FilterProcessor;
import org.apache.camel.processor.Splitter;
import org.apache.camel.support.ServiceSupport;
import org.apache.camel.support.XMLTokenExpressionIterator;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component("namespace")
public class Namespace {
	public void add(String prefix, String uri, String name, Exchange exchange){
		Processor processor = exchange.getContext().getProcessor(name);
		if(processor instanceof  ChoiceProcessor){
			ChoiceProcessor choiceProcessor = (ChoiceProcessor)processor;
			for(FilterProcessor filter: choiceProcessor.getFilters()){
				if(filter.getPredicate() instanceof XPathBuilder){
					XPathBuilder xpath = (XPathBuilder)filter.getPredicate();
					xpath.getNamespaceContext().getNamespaces().put(prefix, uri);
				}
			}
		}else if(processor instanceof  Splitter){
			Splitter splitter = (Splitter) processor;
			((XMLTokenExpressionIterator) splitter.getExpression()).getNamespaces().put(prefix,uri);
		}
	}


}
