package com.claro.wfm.config;

import java.io.InputStream;

import com.claro.wfm.cxf.SoapServiceProvider;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.apache.camel.CamelContext;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.camel.component.kafka.KafkaComponent;
import org.apache.camel.component.mongodb.MongoDbComponent;
import org.apache.camel.component.rabbitmq.RabbitMQComponent;
import org.apache.camel.model.RoutesDefinition;
import org.apache.camel.spring.CamelBeanPostProcessor;
import org.apache.camel.spring.SpringCamelContext;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CamelConfiguration {
	Logger logger = LoggerFactory.getLogger(CamelConfiguration.class);
	
	@Autowired
    private ApplicationContext applicationContext;
	
	@Value("${outbound.context-root}")
	private String contextRoot;
	@Value("${outbound.camel.route}")
	private String route;
	@Value("${outbound.endpoint.wsdl}")
	private String endpointWsdl;
	@Value("${outbound.endpoint.name}")
	private String endpointName;
	@Value("${outbound.endpoint.service}")
	private String endpointService;
	@Value("${outbound.endpoint.address}")
	private String endpointAddress;
	@Value("${outbound.endpoint.address-no-secure}")
	private String endpointAddressNoSecure;
	@Value("${kafka.bootstrap-servers}")
	private String kafkaServer;
	@Value("${spring.rabbitmq.host}")
	private String rabbitmqHostname;
	@Value("${spring.rabbitmq.port}")
	private int rabbitmqPort;
	@Value("${spring.rabbitmq.username}")
	private String rabbitmqUsername;
	@Value("${spring.rabbitmq.password}")
	private String rabbitmqPassword;
	
	@Bean(name="endpointSOAP")
    public CxfEndpoint endpointSOAP(){
		CxfEndpoint cxfEndpoint = createEndpoint(endpointAddressNoSecure);
        return cxfEndpoint;
    }
	
	private CxfEndpoint createEndpoint(String address) {
		CxfEndpoint cxfEndpoint = new CxfEndpoint();
        cxfEndpoint.setAddress(address);
        cxfEndpoint.setWsdlURL(endpointWsdl);
        cxfEndpoint.setServiceClass(SoapServiceProvider.class);
        cxfEndpoint.setEndpointNameString(endpointName);
        cxfEndpoint.setServiceNameString(endpointService);
        return cxfEndpoint;
	}
		
	@Bean(destroyMethod = "stop")
    public CamelContext camelContext() throws Exception {
		
		SpringCamelContext camelContext = new SpringCamelContext(applicationContext);
		InputStream is = this.getClass().getResourceAsStream(route.replace("classpath:", "/"));
        RoutesDefinition routes = camelContext.loadRoutesDefinition(is);
        camelContext.addRouteDefinitions(routes.getRoutes());
        camelContext.setUseMDCLogging(true);
        camelContext.setUnitOfWorkFactory(MDCCustomUnitOfWork::new);
        // KafkaComponent kafka = new KafkaComponent();
        // kafka.setBrokers(kafkaServer);
        // camelContext.addComponent("kafka", kafka);
        RabbitMQComponent rabbitMQ = new RabbitMQComponent();
        rabbitMQ.setHostname(rabbitmqHostname);
        rabbitMQ.setPortNumber(rabbitmqPort);
        rabbitMQ.setUsername(rabbitmqUsername);
        rabbitMQ.setPassword(rabbitmqPassword);
        rabbitMQ.setCamelContext(camelContext);
        camelContext.addComponent("rabbitmq", rabbitMQ);

		MongoDbComponent mongoDB = new MongoDbComponent();
		camelContext.addComponent("mongodb", mongoDB);
		return camelContext;
    }



    @Bean
	public Mongo mongodb(){
		MongoClientURI mongoCli = new MongoClientURI("mongodb://localhost:27017/outbound");
		MongoClient mongodb = new MongoClient(mongoCli);
		return mongodb;
	}

	@Bean
    public CamelBeanPostProcessor camelBeanPostProcessor(CamelContext camelContext, ApplicationContext applicationContext) {
        CamelBeanPostProcessor processor = new CamelBeanPostProcessor();
        processor.setCamelContext(camelContext);
        processor.setApplicationContext(applicationContext);
        return processor;
    }
	
	@Bean
	public ServletRegistrationBean cxfServletRegistrationBean() {
		ServletRegistrationBean registration = new ServletRegistrationBean(new CXFServlet(), contextRoot+"/soap/*");
		registration.setName("CXFServlet");
		return registration;
	}
	
}
