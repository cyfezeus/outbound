package com.claro.wfm.domain;

import lombok.Data;
import lombok.Value;

import java.util.Date;

@Data
public class MessageEvent {
    private Long id;
    private String data;
    private Date date;
    private Status status;

    public static enum Status {
        RECEIVED,
        PROCESSING,
        PROCESSED,
        DROPPED
    }
}

