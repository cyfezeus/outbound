package com.claro.wfm.domain;

import lombok.Data;

@Data
public class UserAuthentication {
	private Long id;
	private String user;
	private String password;
}
