package com.claro.wfm.mock;

import com.claro.wfm.utils.EncryptionAlgorithm;

import toatech.agent.UserT;

public class Test2 {

	
	public static void main(String[] args) {
		
		UserT user = new UserT();
		user.setLogin("soap");
		user.setCompany("in132");
		user.setNow("2014-01-10T13:56:50Z");
		
		String authString = EncryptionAlgorithm.sha256(user.getNow().concat(EncryptionAlgorithm.sha256("secret123".concat(EncryptionAlgorithm.sha256(user.getLogin())))));
		
		System.out.println(authString);
		
	}
	
}

